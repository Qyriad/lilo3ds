﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Qyriad.Lilo3DS
{
	internal sealed class CGFX : Qyriad.Lilo3DS.File
	{
		private sealed class Header
		{
			// ReSharper disable InconsistentNaming
			private readonly Offset MAGIC_OFFSET;
			private const int MAGIC_LENGTH = 0x4;
			private readonly Offset BYTE_ORDER_MARK_OFFSET;
			private const int BYTE_ORDER_MARK_LENGTH = 0x2;
			private readonly Offset HEADER_SIZE_OFFSET;
			private const int HEADER_SIZE_LENGTH = 0x2;
			private readonly Offset FILE_SIZE_OFFSET;
			private const int FILE_SIZE_LENGTH = 0x4;
			// ReSharper restore InconsistentNaming

			internal int Size { get; }

			internal Header(byte[] cgfxFile, CGFX containerCgfx)
			{
				// Instanciate 'constants'
				this.MAGIC_OFFSET = new Offset(0x0, containerCgfx);
				this.BYTE_ORDER_MARK_OFFSET = new Offset(0x4, containerCgfx);
				this.HEADER_SIZE_OFFSET = new Offset(0x6, containerCgfx);
				this.FILE_SIZE_OFFSET = new Offset(0xC, containerCgfx);

				string magic = Utils.GetHexValue(cgfxFile, this.MAGIC_OFFSET.Value, MAGIC_LENGTH, HexOutputType.AsASCIIString);
				if(magic != MAGIC)
				{
					throw new Exception("CGFX Header Magic Exception");
				}

				string byteOrderMark = Utils.GetHexValue(cgfxFile, this.BYTE_ORDER_MARK_OFFSET.Value, BYTE_ORDER_MARK_LENGTH, HexOutputType.AsHexString);
				if(byteOrderMark == "FFFE")
				{
					containerCgfx.Endianness = Endianness.LittleEndian;
				}
				else if(byteOrderMark == "FEFF")
				{
					containerCgfx.Endianness = Endianness.BigEndian;
				}
				else
				{
					throw new Exception("CGFX Header Endianness Exception");
				}

				this.Size = (int) Utils.GetHexValue(cgfxFile, this.HEADER_SIZE_OFFSET.Value, HEADER_SIZE_LENGTH);
				containerCgfx.Size = Utils.GetHexValue(cgfxFile, this.FILE_SIZE_OFFSET.Value, FILE_SIZE_LENGTH);
			}
		}

		internal sealed class DATA : Qyriad.Lilo3DS.File
		{
			private sealed class _Header
			{
				private readonly Offset MAGIC_OFFSET;
				private const int MAGIC_LENGTH = 0x4;
				private readonly Offset DATA_SIZE_OFFSET;
				private const int DATA_SIZE_LENGTH = 0x4;
				private Offset ENTRY_COUNT_OFFSET(int n) => new Offset(0x8 + (n * 8), this.Container);
				private const int ENTRY_COUNT_LENGTH = 0x4;
				private Offset DICT_OFFSET_OFFSET(int n) => new Offset(0xC + (n * 8), this.Container);
				private const int DICT_OFFSET_LENGTH = 0x4;

				private readonly DATA Container;

				internal _Header(byte[] dataSection, DATA container)
				{
					this.Container = container;

					// Instantiate 'constants'
					this.MAGIC_OFFSET = new Offset(0x0, this.Container);
					this.DATA_SIZE_OFFSET = new Offset(0x4, this.Container);

					string Magic = Utils.GetHexValue(dataSection, this.MAGIC_OFFSET.Value, MAGIC_LENGTH, HexOutputType.AsASCIIString);
					if(Magic != this.Container.Magic)
					{
						throw new Exception("DATA Header Magic Exception");
					}

					this.Container.Size = (int) Utils.GetHexValue(dataSection, this.DATA_SIZE_OFFSET.Value, DATA_SIZE_LENGTH);
					this.Container.DICTs = new _DICT[16];
					for(int i = 0; i < 16; i++)
					{
						int entryCount = (int) Utils.GetHexValue(dataSection, this.ENTRY_COUNT_OFFSET(i), ENTRY_COUNT_LENGTH);
						Offset o = new Offset((int) Utils.GetHexValue(dataSection, this.DICT_OFFSET_OFFSET(i), DICT_OFFSET_LENGTH) + this.DICT_OFFSET_OFFSET(i), this.Container);
						this.Container.DICTs[i] = new _DICT(dataSection.Skip(o.Value).ToArray(), entryCount, o, (_DICT._EntryType) i);
					}
				}
			}

			private sealed class _DICT : Qyriad.Lilo3DS.File
			{
				internal enum _EntryType
				{
					Model = 0,
					Texture = 1,
					LUTS = 2,
					Unknown3 = 3,
					Unknown4 = 4,
					Camera = 5,
					Lighting = 6,
					Fog = 7,
					Environment = 8,
					SkeletonAnimation = 9,
					TextureAnimation = 10,
					UnknownAnimation = 11,
					Unknown12 = 12,
					Unknown13 = 13,
					Unknown14 = 14,
					Unknown15 = 15
				}

				private class _Header
				{
					private readonly Offset MAGIC_OFFSET;
					private const int MAGIC_LENGTH = 0x4;
					private readonly Offset SIZE_OFFSET;
					private const int SIZE_LENGTH = 0x4;
					private readonly Offset ENTRY_COUNT_OFFSET;
					private const int ENTRY_COUNT_LENGTH = 0x4;

					private readonly _DICT Container;

					internal _Header(byte[] dictSection, _DICT container, _EntryType Type)
					{
						this.Container = container;
						// Initilize readonly fields
						this.MAGIC_OFFSET = new Offset(0x0, this.Container);
						this.SIZE_OFFSET = new Offset(0x4, this.Container);
						this.ENTRY_COUNT_OFFSET = new Offset(0x8, this.Container);

						string Magic = Utils.GetHexValue(dictSection, this.MAGIC_OFFSET.Value, MAGIC_LENGTH, HexOutputType.AsASCIIString);
						if(Magic != this.Container.Magic)
						{
							throw new Exception("DICT Header Magic Exception");
						}
						this.Container.Size = (int) Utils.GetHexValue(dictSection, this.SIZE_OFFSET.Value, SIZE_LENGTH);
						this.Container.EntryCount = (int) Utils.GetHexValue(dictSection, this.ENTRY_COUNT_OFFSET, ENTRY_COUNT_LENGTH);
					}
				}

				internal class _Entry : File
				{
					private readonly Offset SYMBOL_SELF_RELATIVE_OFFSET_OFFSET;
					private const int SYMBOL_OFFSET_LENGTH = 0x4;
					private readonly Offset OBJECT_SELF_RELATIVE_OFFSET_OFFSET;
					private const int OBJECT_OFFSET_LENGTH = 0x4;

					//internal Offset Offset { get; private set; }
					internal Offset SymbolOffset { get; private set; }
					internal Offset ObjectOffset { get; private set; }

					//private _DICT Container;

					internal _Entry(byte[] dictSection, Offset offset) : base(offset)
					{
						//this.Container = container;

						// Initalize constants
						this.SYMBOL_SELF_RELATIVE_OFFSET_OFFSET = new Offset(0x8, this);
						this.OBJECT_SELF_RELATIVE_OFFSET_OFFSET = new Offset(0xC, this);
						this.Offset = offset;

						// Get symbol offset relative to 'this' entry
						int symbolSelfRelativeOffset = (int) Utils.GetHexValue(dictSection, this.Offset.Value + this.SYMBOL_SELF_RELATIVE_OFFSET_OFFSET.Value, SYMBOL_OFFSET_LENGTH);
						this.SymbolOffset = new Offset(symbolSelfRelativeOffset + SYMBOL_SELF_RELATIVE_OFFSET_OFFSET.Value, this);

						// Get object offset relative to 'this' entry
						int objectSelfRelativeOffset = (int) Utils.GetHexValue(dictSection, this.Offset.Value + this.OBJECT_SELF_RELATIVE_OFFSET_OFFSET.Value, OBJECT_OFFSET_LENGTH);
						this.ObjectOffset = new Offset(objectSelfRelativeOffset + OBJECT_SELF_RELATIVE_OFFSET_OFFSET.Value, this);
					}
				}

				private string Magic = "DICT";
				private _Header Header;
				internal _Entry[] Entries { get; private set; }
				private int Size;
				private int EntryCount;
				internal _DICT(byte[] dictSection, int entryCount, Offset offset, _EntryType type) : base(offset)
				{
					//this.Offset = offset;
					if(type == _EntryType.Texture)
					{
						Console.WriteLine("Texture Dict detected");
						this.Header = new _Header(dictSection, this, type);
						Entries = new _Entry[entryCount];
						for(int i = 0; i < entryCount; i++)
						{
							Entries[i] = new _Entry(dictSection, new Offset(0x1C + (i * 0x10), this));
							//entries[i] = new _Entry(dictSection, this, );
						}
					}
					else
					{
						// Console.WriteLine("Dict not implemented");
					}
				}
			}

			internal sealed class _TXOB : Lilo3DS.File
			{
				/*
				private sealed class _TextureData : Lilo3DS.File
				{
					internal enum TextureFormat
					{
						RGBA8 = 0,
						RGB8,
						RGBA5551,
						RGB565,
						RGBA4,
						LA8,
						HILO8,
						L8,
						A8,
						LA4,
						L4,
						A4,
						ETC1,
						ETC1A4
					}

					internal _TextureData(byte[] section, Offset offset, TextureFormat format) : base(offset)
					{
						//
					}
				}
				*/

				#region Constants
				private Offset MAGIC_OFFSET;
				private const int MAGIC_LENGTH = 0x4;
				private Offset SYMBOL_SELF_RELATIVE_OFFSET_OFFSET;
				private const int SYMBOL_OFFSET_LENGTH = 0x4;
				private Offset HEIGHT_OFFSET;
				private const int HEIGHT_LENGTH = 0x4;
				private Offset WIDTH_OFFSET;
				private const int WIDTH_LENGTH = 0x4;
				private Offset MIPMAP_OFFSET;
				private const int MIPMAP_LENGTH = 0x4;
				private Offset FORMAT_ID_OFFSET;
				private const int FORMAT_ID_LENGTH = 0x4;
				private Offset DATA_SIZE_OFFSET;
				private const int DATA_SIZE_LENGTH = 0x4;
				private Offset DATA_SELF_RELATIVE_OFFSET_OFFSET;
				private const int DATA_OFFSET_LENGTH = 0x4;
				#endregion

				internal string Name { get; private set; }
				internal int TextureHeight { get; private set; }
				internal int TextureWidth { get; private set; }
				internal int MipmapLevels { get; private set; }
				internal IMAG.Texture.TextureFormat TextureFormat { get; private set; }
				internal int TextureDataSize { get; private set; }
				internal Offset TextureDataOffset { get; private set; } // Relative to 'this'

				string Magic = "TXOB";

				internal _TXOB(byte[] section, Offset offset, string name) : base(offset)
				{
					#region Initialize Constants
					this.MAGIC_OFFSET = new Offset(0x4, this);
					this.SYMBOL_SELF_RELATIVE_OFFSET_OFFSET = new Offset(0xC, this);
					this.HEIGHT_OFFSET = new Offset(0x18, this);
					this.WIDTH_OFFSET = new Offset(0x1C, this);
					this.MIPMAP_OFFSET = new Offset(0x28, this);
					this.FORMAT_ID_OFFSET = new Offset(0x34, this);
					this.DATA_SIZE_OFFSET = new Offset(0x44, this);
					this.DATA_SELF_RELATIVE_OFFSET_OFFSET = new Offset(0x48, this);
					#endregion

					string magic = Utils.GetHexValue(section, this.MAGIC_OFFSET.Value, MAGIC_LENGTH, HexOutputType.AsASCIIString);
					if(magic != this.Magic)
					{
						throw new Exception("TXOB Magic Exception");
					}
					// Relative to 'this'
					Offset symbolOffset = new Offset((SYMBOL_SELF_RELATIVE_OFFSET_OFFSET.Value + (int) Utils.GetHexValue(section, this.SYMBOL_SELF_RELATIVE_OFFSET_OFFSET, SYMBOL_OFFSET_LENGTH)), this);
					string symbol = Utils.ReadString(section, symbolOffset.Value);
					if(symbol != name)
					{
						throw new Exception("TXOB Symbol Mismatch Exception");
					}
					this.Name = name;

					this.TextureHeight = (int) Utils.GetHexValue(section, this.HEIGHT_OFFSET.Value, HEIGHT_LENGTH);
					this.TextureWidth = (int) Utils.GetHexValue(section, this.WIDTH_OFFSET.Value, WIDTH_LENGTH);
					this.MipmapLevels = (int) Utils.GetHexValue(section, this.MIPMAP_OFFSET.Value, MIPMAP_LENGTH);
					this.TextureFormat = (IMAG.Texture.TextureFormat) Utils.GetHexValue(section, this.FORMAT_ID_OFFSET.Value, FORMAT_ID_LENGTH);
					this.TextureDataSize = (int) Utils.GetHexValue(section, this.DATA_SIZE_OFFSET, DATA_SIZE_LENGTH);
					this.TextureDataOffset = new Offset(DATA_SELF_RELATIVE_OFFSET_OFFSET.Value + (int) Utils.GetHexValue(section, this.DATA_SELF_RELATIVE_OFFSET_OFFSET, DATA_OFFSET_LENGTH), this);
				}
			}

			private readonly string Magic = "DATA";
			private readonly _Header Header;
			internal int Size { get; private set; }
			private _DICT[] DICTs;
			internal _TXOB[] TXOBs { get; private set; }

			internal DATA(byte[] dataSection, Offset offset) : base(offset)
			{
				this.Offset = offset;
				this.Header = new _Header(dataSection, this);

				// Get how many TXOBs were pointed to in the second DICT
				_DICT textureDict = this.DICTs[1];
				this.TXOBs = new _TXOB[textureDict.Entries.Length];
				for(int i = 0; i < textureDict.Entries.Length; i++)
				{
					// Get TXOB offset
					Offset symbolOffset = textureDict.Entries[i].SymbolOffset; // Relative to Entry
																			   // Get relative to 'this' (DATA) // Entry->DICT->DATA
					symbolOffset = symbolOffset.Unwind().Unwind();
					string name = Utils.ReadString(dataSection, symbolOffset.Value);
					Offset objectOffset = textureDict.Entries[i].ObjectOffset; // Relative to Entry
																			   // Get relative to 'this' (DATA) // Entry->DICT->DATA
					objectOffset = objectOffset.Unwind().Unwind();
					this.TXOBs[i] = new _TXOB(dataSection.Skip(objectOffset.Value).ToArray(), objectOffset, name);
				}
			}
		}

		internal sealed class IMAG : Lilo3DS.File
		{
			[DebuggerDisplay("{Red}, {Green}")]
			struct Pixel32
			{
				public byte Red;
				public byte Green;
				public byte Blue;
				public byte Alpha;

				public Pixel32(byte red, byte green, byte blue, byte alpha)
				{
					this.Red = red;
					this.Green = green;
					this.Blue = blue;
					this.Alpha = alpha;
				}
			}
			internal sealed class Texture : Lilo3DS.File
			{
				internal enum TextureFormat
				{
					RGBA8 = 0,
					RGB8,
					RGBA5551,
					RGB565,
					RGBA4,
					LA8,
					HILO8,
					L8,
					A8,
					LA4,
					L4,
					A4,
					ETC1,
					ETC1A4
				}

				private byte[] Data;
				internal string Name { get; private set; }
				internal int Height { get; private set; }
				internal int Width { get; private set; }
				internal int MipmapLevels { get; private set; }
				internal TextureFormat Format { get; private set; }
				internal int Size { get; private set; }

				internal Texture(byte[] section, Offset offset, DATA._TXOB describingTXOB) : base(offset)
				{
					this.Data = section;
					this.Name = describingTXOB.Name;
					this.Height = describingTXOB.TextureHeight;
					this.Width = describingTXOB.TextureWidth;
					this.MipmapLevels = describingTXOB.MipmapLevels;
					this.Format = describingTXOB.TextureFormat;
					this.Size = describingTXOB.TextureDataSize;

					if(this.Format == TextureFormat.RGBA8)
					{
						List<Pixel32> pixels = new List<Pixel32>();
						for(int i = 0; i < section.Length; i += 4)
						{
							// AAAA AAAA BBBB BBBB GGGG GGGG RRRR RRRR; 4 bytes
							Pixel32 p = new Pixel32();
							p.Alpha = section[i];
							p.Blue = section[i + 1];
							p.Green = section[i + 2];
							p.Red = section[i + 3];
							pixels.Add(p);
						}
					}
				}
			}

			internal Texture[] Textures { get; private set; }

			internal IMAG(byte[] section, Offset offset, DATA._TXOB[] TXOBs) : base(offset)
			{
				if(offset.RelativeTo.GetType() != typeof(CGFX))
				{
					throw new Exception("IMAG Container Mismatch Exception");
				}

				this.Offset = offset;
				this.Container = this.Offset.RelativeTo;

				this.Textures = new Texture[TXOBs.Length];

				for(int i = 0; i < TXOBs.Length; i++)
				{
					int texSize = TXOBs[i].TextureDataSize;
					// Offset relative to CGFX - this.Offset (_DATA size)
					Offset texOffset = new Offset(TXOBs[i].TextureDataOffset.Unwind().Unwind().Value - this.Offset.Value, this); // Relative to IMAG
																																 //this.Textures[i] = new Texture(section.Skip(texOffset.Value).ToArray(), texOffset, TXOBs[i]);
					byte[] texData = section.Skip(texOffset.Value).ToArray();
					this.Textures[i] = new Texture(texData, texOffset, TXOBs[i]);
				}
			}
		}

		private const string MAGIC = "CGFX";

		Endianness Endianness;
		internal long Size { get; private set; }

		private readonly Header _header;
		private readonly DATA _DATA;
		//private readonly IMAG _IMAG; 09/13/2017
		public readonly IMAG _IMAG;


		internal CGFX(byte[] cgfxData, Offset offset) : base(offset)
		{
			this.Offset = offset;
			this.Container = this.Offset.RelativeTo;
			this._header = new Header(cgfxData, this);
			this._DATA = new DATA(cgfxData.Skip(this._header.Size).ToArray(), new Offset(this._header.Size, this));
			this._IMAG = new IMAG(cgfxData.Skip(this._DATA.Size + this._header.Size).ToArray(), new Offset(this._DATA.Size + this._header.Size, this), this._DATA.TXOBs);
		}
	}
}
