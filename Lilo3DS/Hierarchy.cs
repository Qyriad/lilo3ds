﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lilo3DS
{
	public class Hierarchy<T>
	{
		protected internal T Value
		{
			get
			{
				return this.Tuple.Item1;
			}
		}
		protected internal Hierarchy<T> SubHierarchy
		{
			get
			{
				return this.Tuple.Item2;
			}
		}

		//protected internal Dictionary<T, Hierarchy<T>> Dictionary;
		protected internal Tuple<T, Hierarchy<T>> Tuple;

		public Hierarchy(T value)
		{
			//
		}

		internal Hierarchy(T value, Hierarchy<T> subHierarchy)
		{
			//this.Value = value;
			//this.SubHierarchy = subHierarchy;
			//this.Dictionary.Add(value, subHierarchy);
			this.Tuple = new Tuple<T, Hierarchy<T>>(value, subHierarchy);
			Console.WriteLine();
		}
	}
}
