﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lilo3DS
{
	enum PixelFormat
	{
		RGBA8,  // AAAA AAAA BBBB BBBB GGGG GGGG RRRR RRRR; 4 bytes
		BGR8,   // BBBB BBBB GGGG GGGG RRRR RRRR; 3 bytes
		RGB565, // GGGB BBBB RRRR RGGG (literally); 2 bytes
		        // RRRR RGGG GGGB BBBB (after reading as 16bit int in little endian)
		RGBA444,// BBBB AAAA RRRR GGGG (literally); 2 bytes
		        // RRRR GGGG BBBB AAAA (after reading as 16bit int in little endian)
		LA8,    // AAAA AAAA LLLL LLLL (Grayscale with transparency)
		HILO8,  // Apparently the same as below?
		L8,     // LLLL LLLL (Grayscale)
		A8,     // AAAA AAAA? An alpha only image? Seriously? Why?
		LA4,    // AAAA LLLL (Packed grayscale with transparency)
		L4,     // LLLL 0.5bbp? Is that really necessary? FOUR shades of grey. What are you going to use that for. 
		// Not supported yet
		ETC1,
		ETCA, 
	}

	class TextureConverter
	{
		public static Bitmap TranscodeToARGB8(byte[] textureData, PixelFormat sourceFormat) // BGRA
		{
			List<string> a = new List<string>();
			IEnumerable<int> b  = from o in a select a.Count;
			if(sourceFormat == PixelFormat.ETC1 || sourceFormat == PixelFormat.ETCA)
			{
				throw new NotImplementedException();
			}
			else
			{
				//
			}

			return null;
		}
	}
}


#region Convert Image
/*
		private byte[] ETC1Decompress(byte[] data, byte[] alphas, int width, int height)
		{
			byte[] Out = new Byte[(width * height * 4)];
			int offset = 0;
			for(int y = 0; y < ((height / 4) - 1); y++)
			{
				for(int x = 0; x < ((width / 4) - 1); x++)
				{
					byte[] block = new byte[8];
					byte[] alphasBlock = new byte[8];
					for(int i = 0; i <= 7; i++)
					{
						block[7 - i] = (byte) Utils.GetHexValue(data, offset + i, 0x1);
						alphasBlock[i] = alphas[offset + i];
					}
					offset += 8;
					block = ETC1DecompressBlock(block);

					bool lowHighToggle = false;
					int alphaOffset = 0;
					for(int tx = 0; tx <= 3; tx++)
					{
						for(int ty = 0; ty <= 3; ty++)
						{
							int outOffset = (x * 4 + tx + ((y * 4 + ty) * width)) * 4;
							int blockOffset = (tx + (ty * 4)) * 4;
							Buffer.BlockCopy(block, blockOffset, Out, outOffset, 3);

							int alphaData;
							if(lowHighToggle)
							{
								alphaData = (alphasBlock[alphaOffset] & 0xF0) >> 4;
								alphaOffset += 1;
							}
							else
							{
								alphaData = (alphasBlock[alphaOffset] & 0xF);
							}
							lowHighToggle = !lowHighToggle;
							Out[outOffset + 3] = Convert.ToByte((alphaData << 4) + alphaData);
						}
					}
				}
			}
			return Out;
		}

		private byte[] ETC1DecompressBlock(byte[] data)
		{
			// Ericsson Texture compression
			int blockTop = (int) Utils.GetHexValue(data, 0, 0x4);
			int blockBottom = (int) Utils.GetHexValue(data, 4, 0x4);
			bool flip = (blockTop & 0x1000000) > 0;
			bool diff = (blockTop & 0x2000000) > 0;
			int r1, g1, b1, r2, g2, b2;
			int r, g, b;

			if(diff)
			{
				r1 = blockTop & 0xF8;
				g1 = (blockTop & 0xF800) >> 8;
				b1 = (blockTop & 0xF80000) >> 16;

				r = ToSignedByte(Convert.ToByte(r1 >> 3)) + (ToSignedByte(Convert.ToByte((blockTop & 7) << 5)) >> 5);
				g = ToSignedByte(Convert.ToByte(g1 >> 3)) + (ToSignedByte(Convert.ToByte((blockTop & 0x700) >> 3)) >> 5);
				b = ToSignedByte(Convert.ToByte(b1 >> 3)) + (ToSignedByte(Convert.ToByte((blockTop & 0x70000) >> 11)) >> 5);

				r2 = r;
				g2 = g;
				b2 = b;

				r1 = r1 + (r1 >> 5);
				g1 = g1 + (g1 >> 5);
				b1 = b1 + (b1 >> 5);

				r2 = (r2 << 3) + (r2 >> 2);
				g2 = (g2 << 3) + (g2 >> 2);
				b2 = (b2 << 3) + (b2 >> 2);
			}
			else
			{
				r1 = blockTop & 0xF0;
				r1 = r1 + (r1 >> 4);
				g1 = (blockTop & 0xF000) >> 8;
				g1 = g1 + (g1 >> 4);
				b1 = (blockTop * 0xF00000) >> 16;
				b1 = b1 + (b1 >> 4);

				r2 = (blockTop & 0xF) << 4;
				r2 = r2 + (r2 >> 4);
				g2 = (blockTop & 0xF00) >> 4;
				g2 = g2 + (g2 >> 4);
				b2 = (blockTop & 0xF0000) >> 12;
				b2 = b2 + (b2 >> 4);
			}

			int modTable1 = (blockTop >> 29) & 7;
			int modTable2 = (blockTop >> 26) & 7;

			byte[] Out = new byte[(4 * 4 * 4) - 1];
			if(!flip)
			{
				for(int y = 0; y <= 3; y++)
				{
					for(int x = 0; x <= 1; x++)
					{
						Color col1 = ModifyPixel(r1, g1, b1, x, y, blockBottom, modTable1);
						Color col2 = ModifyPixel(r2, g2, b2, x + 2, y, blockBottom, modTable2);
						Out[(y * 4 + x) * 4] = col1.G;
						Out[((y * 4 + x) * 4) + 1] = col1.G;
						Out[((y * 4 + x) * 4) + 2] = col1.B;
						Out[(y * 4 + x + 2) * 4] = col2.R;
						Out[((y * 4 + x + 2) * 4) + 1] = col2.G;
						Out[((y * 4 + x + 2) * 4) + 2] = col2.B;
					}
				}
			}
			else
			{
				for(int y = 0; y <= 1; y++)
				{
					for(int x = 0; x <= 3; x++)
					{
						Color col1 = ModifyPixel(r1, g1, b1, x, y, blockBottom, modTable1);
						Color col2 = ModifyPixel(r2, g2, b2, x, y + 2, blockBottom, modTable2);
						Out[(y * 4 + x) * 4] = col1.R;
						Out[((y * 4 + x) * 4) + 1] = col1.G;
						Out[((y * 4 + x) * 4) + 2] = col1.B;
						Out[((y + 2) * 4 + x) * 4] = col2.R;
						Out[(((y + 2) * 4 + x) * 4) + 1] = col2.G;
						Out[(((y + 2) * 4 + x) * 4) + 2] = col2.B;
					}
				}
			}
			return Out;
		}

		private Color ModifyPixel(int r, int g, int b, int x, int y, int ModBlock, int ModTable)
		{
			int index = x * 4 + y;
			int pixelModulation;
			int msb = ModBlock << 1;

			if(index < 8)
			{
				pixelModulation = Utils.ModulationTable[ModTable][((ModBlock >> (index + 24)) & 1) + ((msb >> (index + 8)) & 2)];
			}
			else
			{
				pixelModulation = Utils.ModulationTable[ModTable][((ModBlock >> (index + 8)) & 1) + ((msb >> (index - 8)) & 2)];
			}

			r = Clip(r + pixelModulation);
			g = Clip(g + pixelModulation);
			b = Clip(b + pixelModulation);
			return Color.FromArgb(b, g, r);

		}

		private byte Clip(int value)
		{
			if(value > 0xFF)
			{
				return 0xFF;
			}
			else if(value < 0)
			{
				return 0;
			}
			else
			{
				return Convert.ToByte(value & 0xFF);
			}
		}

		private int[] GetETC1Scramble(int width, int height)
		{
			int[] tileScramble = new int[((width / 4) * (height / 4))];
			int baseAcc = 0;
			int lineAcc = 0;
			int baseNum = 0;
			int lineNum = 0;

			for(int tile = 0; tile < tileScramble.Length; tile++)
			{
				if((tile % (width / 4) == 0) && tile > 0)
				{
					if(lineAcc < 1)
					{
						lineAcc += 1;
						lineNum += 2;
						baseNum = lineNum;
					}
					else
					{
						lineAcc = 0;
						baseNum -= 2;
						lineNum = baseNum;
					}
				}

				tileScramble[tile] = baseNum;

				if(baseAcc < 1)
				{
					baseAcc += 1;
					baseNum += 1;
				}
				else
				{
					baseAcc = 0;
					baseNum += 3;
				}
			}
			return tileScramble;
		}

		private sbyte ToSignedByte(byte byteToConvert)
		{
			if(byteToConvert < 0x80)
				return Convert.ToSByte(byteToConvert);
			else
				return Convert.ToSByte(byteToConvert - 0x100);
		}

		/*
		public void GetTextures()
		{
			bool lowHighToggle = false;
			for(int txob = 0; txob < this.Data.GetDict((int) DictType.Textures).GetNumberOfEntries(); txob++)
			{
				int height = this.Data.GetTxob(txob).GetHeight();
				int width = this.Data.GetTxob(txob).GetWidth();
				int format = (int) this.Data.GetTxob(txob).GetTextureFormat();
				//int offset = this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.GetSize() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET;
				//offset += (int) Utils.GetHexValue(this.cgfx, offset, 0x2);//this.Data.GetTxob(i).GetTextureDataSelfRelativeOffset();
				int offset = this.Data.GetTxob(txob).GetTextureDataSelfRelativeOffset() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET + this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.Size;
				Byte[] outByte = new Byte[(this.Data.GetTxob(txob).GetWidth() * this.Data.GetTxob(txob).GetHeight() * 4)];
				if(this.Data.GetTxob(txob).GetTextureFormat() == TextureFormat.ETC1 || this.Data.GetTxob(txob).GetTextureFormat() == TextureFormat.ETCA4)
				{
					//offset = this.Data.GetTxob(txob).GetTextureDataSelfRelativeOffset() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET + this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.GetSize();
					//offset = this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.GetSize();
					//offset = this.Data.GetTxob(txob).GetTextureDataSelfRelativeOffset() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET + this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.GetSize();
					//throw new Exception("Not yet implemented");
					byte[] tempBuffer = new byte[((width * height) / 2)];
					byte[] alphas = new byte[tempBuffer.Length];
					if(format == 12)
					{
						Buffer.BlockCopy(this.cgfx, offset, tempBuffer, 0, tempBuffer.Length);
						for(int j = 0; j < alphas.Length; j++)
						{
							alphas[j] = 0xFF;
						}
					}
					else
					{
						int k = 0;
						for(int j = 0; j < (width * height) - 1; j++)
						{
							Buffer.BlockCopy(this.cgfx, offset + j + 8, tempBuffer, k, 8);
							Buffer.BlockCopy(this.cgfx, offset + j, alphas, k, 8);
							k += 8;
							j += 15;
						}
					}
					byte[] temp2 = ETC1Decompress(tempBuffer, alphas, width, height);

					// Tiles with ETC compress on 3DS are scrabled
					int[] tileScramble = GetETC1Scramble(width, height);
					int i = 0;
					for(int tiley = 0; tiley < (height / 4); tiley++)
					{
						for(int tilex = 0; tilex < (width / 4); tilex++)
						{
							int tx = tileScramble[i] % (width / 4);
							int ty = (tileScramble[i] - tx) / (width / 4);
							for(int y = 0; y <= 3; y++)
							{
								for(int x = 0; x <= 3; x++)
								{
									int outOffset = ((tilex * 4) + x + (((height - 1) - ((tiley * 4) + y)) * width)) * 4;
									int imageOffset = ((tx * 4) + x + (((ty * 4) + y) * width)) * 4;

									outByte[outOffset] = temp2[imageOffset];
									outByte[outOffset + 1] = temp2[imageOffset + 1];
									outByte[outOffset + 2] = temp2[imageOffset + 2];
									outByte[outOffset + 3] = temp2[imageOffset + 3];
								}
							}
							i += 1;
						}
					}

				}
				else
				{
					//offset = this.Data.GetTxob(txob).GetTextureDataSelfRelativeOffset() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET + this.Data.GetTxob(txob).GetDATARelativeOffset() + this.Header.GetSize();
					//int offset = this.Data.GetTxob(i).GetDATARelativeOffset() + Txob.TEXTURE_DATA_SELF_RELATIVE_OFFSET_OFFSET + this.Data.GetTxob(i).GetTextureDataSelfRelativeOffset(); //txobDataOffset;

					// If someone can explain this section of code to me please do

					for(int tileY = 0; tileY < (height / 8); tileY++)
					{
						for(int tileX = 0; tileX < (width / 8); tileX++)
						{
							for(int i = 0; i < 64; i++)
							{
								int x = Utils.TileOrder[i] % 8;
								int y = (Utils.TileOrder[i] - x) / 8;
								int outOffset = ((tileX * 8) + x + (((height - 1) - ((tileY * 8) + y)) * width)) * 4;
								int PixelData = 0;
								switch(format)
								{
									case 0: // R8G8B8A8
										Buffer.BlockCopy(this.cgfx, offset + 1, outByte, outOffset, 3);
										outByte[outOffset + 3] = this.cgfx[offset];
										offset += 4;
										break;
									case 1: // R8G8B8 (Semi-transparent)
										Buffer.BlockCopy(this.cgfx, offset, outByte, outOffset, 3);
										outByte[outOffset + 3] = 0xFF;
										offset += 3;
										break;
									case 2: // R5G5B5A1
										PixelData = (int) Utils.GetHexValue(this.cgfx, offset, 0x2);
										outByte[outOffset + 2] = Convert.ToByte(((PixelData >> 11) & 0x1F) * 8);
										outByte[outOffset + 1] = Convert.ToByte(((PixelData >> 6) & 0x1F) * 8);
										outByte[outOffset] = Convert.ToByte(((PixelData >> 1) & 0x1F) * 8);
										outByte[outOffset + 3] = Convert.ToByte((PixelData & 1) * 0xFF);
										offset += 2;
										break;
									case 3: // RGB565
										PixelData = (int) Utils.GetHexValue(this.cgfx, offset, 0x2);
										outByte[outOffset + 2] = Convert.ToByte(((PixelData >> 11) & 0x1F) * 8);
										outByte[outOffset + 1] = Convert.ToByte(((PixelData >> 5) & 0x3F) * 4);
										outByte[outOffset] = Convert.ToByte(((PixelData) & 0x1F) * 8);
										outByte[outOffset + 3] = 0xFF;
										offset += 2;
										break;
									case 4: // RGBA444
										PixelData = (int) Utils.GetHexValue(this.cgfx, offset, 0x2);
										outByte[outOffset + 2] = Convert.ToByte(((PixelData >> 12) & 0xF) * 0x11);
										outByte[outOffset + 1] = Convert.ToByte(((PixelData >> 8) & 0xF) * 0x11);
										outByte[outOffset] = Convert.ToByte(((PixelData >> 4) & 0xF) * 0x11);
										outByte[outOffset + 3] = Convert.ToByte((PixelData & 0xF) * 0x11);
										offset += 2;
										break;
									case 5: // L8A8
											//PixelData = (byte) Utils.GetHexValue(this.cgfx, offset + 1, 0x1);
										byte PixelByte = (byte) Utils.GetHexValue(this.cgfx, offset + 1, 0x1);
										outByte[outOffset] = PixelByte;
										outByte[outOffset + 1] = PixelByte;
										outByte[outOffset + 2] = PixelByte;
										outByte[outOffset + 3] = (byte) Utils.GetHexValue(this.cgfx, offset, 0x1);
										offset += 2;
										break;
									case 6: // HILO8
									case 7: // L8
										outByte[outOffset] = (byte) Utils.GetHexValue(this.cgfx, offset, 0x1);
										outByte[outOffset + 1] = (byte) Utils.GetHexValue(this.cgfx, offset, 0x1);
										outByte[outOffset + 2] = (byte) Utils.GetHexValue(this.cgfx, offset, 0x1);
										outByte[outOffset + 3] = 0xFF;
										offset += 1;
										break;
									case 8: // A8
										outByte[outOffset] = 0xFF;
										outByte[outOffset + 1] = 0xFF;
										outByte[outOffset + 2] = 0xFF;
										outByte[outOffset + 3] = (byte) Utils.GetHexValue(this.cgfx, offset, 0x1);
										offset += 1;
										break;
									case 9: // L4A4
										int luma = (int) Utils.GetHexValue(this.cgfx, offset, 0x1) & 0xF;
										int alpha = ((int) Utils.GetHexValue(this.cgfx, offset, 0x1) & 0xF0) >> 4;
										outByte[outOffset] = Convert.ToByte((luma << 4) + luma);
										outByte[outOffset + 1] = Convert.ToByte((luma << 4) + luma);
										outByte[outOffset + 2] = Convert.ToByte((luma << 4) + luma);
										outByte[outOffset + 3] = Convert.ToByte((alpha << 4) + alpha);
										break;
									case 10: // L4
										PixelData = 0;
										if(lowHighToggle)
										{
											PixelData = (int) Utils.GetHexValue(this.cgfx, offset, 0x1) & 0xF;
											offset += 1;
										}
										else
										{
											PixelData = ((int) Utils.GetHexValue(this.cgfx, offset, 0x1) & 0xF) >> 4;
										}
										outByte[outOffset] = Convert.ToByte(PixelData * 0x11);
										outByte[outOffset + 1] = Convert.ToByte(PixelData * 0x11);
										outByte[outOffset + 2] = Convert.ToByte(PixelData * 0x11);
										outByte[outOffset + 3] = 0xFF;
										lowHighToggle = !lowHighToggle;
										break;
									default:
										throw new Exception("Not yet implemented");
										break;
								}
							}
						}
					}
				}

				Bitmap img = new Bitmap(this.Data.GetTxob(txob).GetWidth(), this.Data.GetTxob(txob).GetHeight(), PixelFormat.Format32bppArgb);
				BitmapData imgData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
				System.Runtime.InteropServices.Marshal.Copy(outByte, 0, imgData.Scan0, outByte.Length);
				img.UnlockBits(imgData);
				img.RotateFlip(RotateFlipType.RotateNoneFlipY);
				img.Save("C:\\Users\\Mikaela\\Desktop\\" + this.Data.GetTxob(txob).GetName() + ".png", ImageFormat.Png);
				Console.WriteLine("Saved!");
				//Console.ReadLine();
			}
			
		}
		*/
#endregion
