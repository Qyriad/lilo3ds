﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Qyriad.Lilo3DS
{
	internal static class Program
	{
		//private const string path = "D:/Games/Emulated/SNESVC/OldSNES.2/castle/ExeFS/banner/banner10.bcmdl";
		private const string path = "C:/Users/Mikaela/Desktop/banner0.cgfx";

		private static int[] TileOrder =
		{
			00, 01, 08, 09, 02, 03, 10, 11,
			16, 17, 24, 25, 18, 19, 26, 27,
			04, 05, 12, 13, 06, 07, 14, 15,
			20, 21, 28, 29, 22, 23, 30, 31,
			32, 33, 40, 41, 34, 35, 42, 43,
			48, 49, 56, 57, 50, 51, 58, 59,
			36, 37, 44, 45, 38, 39, 46, 47,
			52, 53, 60, 61, 54, 55, 62, 63
		};
		internal static void Main(string[] args)
		{
			//Console.WriteLine(TestClass.h);
			//Qyriad.Lilo3DS.CGFX.DATA.Data();
			// Read file
			FileStream st = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); // Don't lock the file
			byte[] cgfxData = new byte[st.Length];
			long len = st.Length;
			st.Read(cgfxData, 0, (int) len);


			/*
			// CGFX Section
			Console.WriteLine("CGFX Header @0x0:");
			Console.WriteLine("\tMagic: " + Utils.GetHexValue(cgfx, 0x0, 0x4, HexOutputType.AsASCIIString));
			Console.WriteLine("\tEndianness: " + Utils.GetHexValue(cgfx, 0x4, 0x2, HexOutputType.AsHexString));
			int cgfxHeaderSize = (int) Utils.GetHexValue(cgfx, 0x6, 0x2);
			Console.WriteLine("\tCFGX Header Size: " + cgfxHeaderSize.ToString("X2"));
			Console.WriteLine("\tVersion: " + Utils.GetHexValue(cgfx, 0x8, 0x4));
			Console.WriteLine("\tFile size: " + Utils.GetHexValue(cgfx, 0xC, 0x4));
			Console.WriteLine("\tEntries: " + Utils.GetHexValue(cgfx, 0x10, 0x4));
			*/
			CGFX cgfx = new CGFX(cgfxData, new Offset(0x0, null));
			//Console.WriteLine(cgfx.IMAG.Textures[0].Name + ": " + cgfx.IMAG.Textures[0].Format.ToString());
			for(int i = 0; i < cgfx._IMAG.Textures.Length; i++)
			{
				Console.WriteLine(cgfx._IMAG.Textures[i].Name + ": " + cgfx._IMAG.Textures[i].Format.ToString());
			}
			Console.ReadLine();
            //cgfxFile.Offset.GetRelativeTo(cgfxFile);
            //Offset o = cgfxFile.Offset.Unwind();
            //cgfxFile.GetTextures();

			return;
			/*

			// DATA Section - Contains a list of dict references
			byte[] data = cgfx.Skip(cgfxHeaderSize).ToArray();

			Console.WriteLine("\nDATA Header @0x" + cgfxHeaderSize.ToString("X2") + ":");
			Console.WriteLine("\tMagic: " + Utils.GetHexValue(data, 0x0, 0x4, HexOutputType.AsASCIIString));
			int dataSize = (int)Utils.GetHexValue(data, 0x4, 0x4);
			Console.WriteLine("\tData Size: " + dataSize.ToString("X2"));

			int[] dictRefs = new int[15]; // Relative to the pointer's location
			int[] dictEntries = new int[15]; // Number of entries each dict has
			for (int i = 0; i < 15; i++)
			{
				dictRefs[i] = (int) Utils.GetHexValue(data, 0xC +(8 * i), 0x4);
				if(dictRefs[i] != 0) // Some dicts don't actually exist
				{
					dictEntries[i] = (int)Utils.GetHexValue(data, 0x8 + (8 * i), 0x4);
					Console.WriteLine("\tDICT " + i.ToString("D2") + " offset (self-relative): " + dictRefs[i].ToString("X2") + ", entries: " + dictEntries[i].ToString("X2"));
				}
			}


			// DICT Section
			byte[][] dicts = new byte[15][];
			int[] dictSizes = new int[15];
			int[] dictOffsets = new int[15]; // Relative to DATA Header
			for(int i = 0; i < 15; i++)
			{
				if(dictRefs[i] == 0)
				{
					dictSizes[i] = 0;
					dictOffsets[i] = 0;
					continue;
				}
				dictOffsets[i] = dictRefs[i] + (0xC + (0x8 * i));
				dicts[i] = data.Skip(dictOffsets[i]).ToArray();
				dictSizes[i] = (int) Utils.GetHexValue(dicts[i], 0x4, 0x4);
				dicts[i] = dicts[i].Take(dictSizes[i]).ToArray();
				Console.WriteLine("\nDICT " + i.ToString("D2") + " @0x" + dictOffsets[i].ToString("X2"));
				Console.WriteLine("\tMagic: " + Utils.GetHexValue(dicts[i], 0x0, 0x4, HexOutputType.AsASCIIString));
				Console.WriteLine("\tSize: " + dictSizes[i]);
				//Console.WriteLine("\tSymbol offset (self relative): " + Utils.GetHexValue(dicts[i], 0x8, 0x4, Endianness.LittleEndian));
				//Console.WriteLine("\tObject offset (self relative): " + Utils.GetHexValue(dicts[i], 0xC, 0x1C, Endianness.LittleEndian));

			}

			
			// CMDL Section
			// Find last dict
			int lastDict = 0;
			for(int i = 0; i < 15; i++)
			{
				lastDict = ((dictRefs[i] == 0) ? (lastDict) : (i));
			}
			// Skip the CGFX Header and the DICTS
			byte[] cmdl = cgfx.Skip(cgfxHeaderSize + dictOffsets[lastDict] + dictSizes[lastDict]).ToArray();
			Console.WriteLine("\nCMDL @0x" + (cgfxHeaderSize + dictOffsets[lastDict] + dictSizes[lastDict]).ToString("X2"));
			Console.WriteLine("\tMagic: " + Utils.GetHexValue(cmdl, 0x4, 0x4, HexOutputType.AsASCIIString));
			Console.WriteLine("\tModel Name Offset (self-relative): " + Utils.GetHexValue(cmdl, 0xC, 0x4).ToString("X2"));
			*/
			/*
			Console.WriteLine("\nTexture DICT Entries: ");
			int textureDictOffset = dictOffsets[1]; // Relative to DATA
			int firstEntryOffset = textureDictOffset + 0x1C; // Relative to DATA
			Console.WriteLine("Entry 0 offset (from data): " + firstEntryOffset.ToString("X2"));
			int nameOffset = 0;
			for(int i = 0; i < dictEntries[1] + 1; i++)
			{
				int entryIOffset = firstEntryOffset + (0x10 * i);
				nameOffset = (int) Utils.GetHexValue(data, firstEntryOffset + 0x8, 0x4, Endianness.LittleEndian);
				Console.WriteLine("Name offset: " + nameOffset.ToString("X2"));
				Console.WriteLine("Name: " + ReadString(data, firstEntryOffset + 0x8 + nameOffset));
				//byte[] entry = dicts[1].Skip(0x1C).ToArray();
			}
			*/
			/*
			Console.WriteLine("\nTexture Entries: ");
			// Get name of texture entries
			int textureDictOffset = dictOffsets[1] + cgfxHeaderSize; // Relative to CGFX
			const int ENTRY_OFFSET = 0x1C; // Relative to DICT header
			const int ENTRY_DISTANCE = 0x10; // Entries are 10 bytes apart
			const int NAME_OFFSET_OFFSET = 0x8; // The nameOffset starts 8 bytes after the entry begins
			for(int entry = 0; entry < 1/*dictEntries[1]*/; /*entry++)
			{
				// Get name
				// Relative to itself|nameOffsetOffset
				int nameOffset = (int) Utils.GetHexValue(cgfx, textureDictOffset + ENTRY_OFFSET + NAME_OFFSET_OFFSET + (ENTRY_DISTANCE * entry), 0x4);
				nameOffset += textureDictOffset + ENTRY_OFFSET + NAME_OFFSET_OFFSET + (ENTRY_DISTANCE * entry); // Now relative to CGFX
				Console.WriteLine(Utils.ReadString(cgfx, nameOffset));

				// Get TXOB offset
				// Relative to itself
				int txobOffset = (int) Utils.GetHexValue(cgfx, textureDictOffset + ENTRY_OFFSET + 0xC + (ENTRY_DISTANCE * entry), 0x4);
				txobOffset += textureDictOffset + ENTRY_OFFSET + 0xC + (ENTRY_DISTANCE * entry);
				const int HEIGHT_OFFSET = 0x18;
				const int WIDTH_OFFSET = 0x1C;
				const int FORMAT_OFFSET = 0x34;
				const int DATA_SIZE_OFFSET = 0x44;
				const int DATA_OFFSET_OFFSET = 0x48;
				Console.WriteLine("TXOB Offset: " + txobOffset.ToString("X2"));
				int width = (int) Utils.GetHexValue(cgfx, txobOffset + HEIGHT_OFFSET, 0x4);
				int height = (int) Utils.GetHexValue(cgfx, txobOffset + WIDTH_OFFSET, 0x4);
				Console.WriteLine("Size: " + width + "x" + height);
				int format = (int) Utils.GetHexValue(cgfx, txobOffset + FORMAT_OFFSET, 0x4);
				Console.WriteLine("Format: " + format);
				int txobDataSize = (int) Utils.GetHexValue(cgfx, txobOffset + DATA_SIZE_OFFSET, 0x4);
				Console.WriteLine("TXOB Data Size: 0x" + txobDataSize.ToString("X2"));
				// Image data offset; relative to itself
				int txobDataOffset = (int) Utils.GetHexValue(cgfx, txobOffset + DATA_OFFSET_OFFSET, 0x4);
				txobDataOffset += txobOffset + DATA_OFFSET_OFFSET;
				Console.WriteLine("TXOB Data Offset: 0x" + txobDataOffset.ToString("X2"));

				// Convert texture
				Byte[] outByte = new Byte[(width * height * 4)];
				bool lowHighToggle = false;
				if(format == 12 || format == 13) // ETC1 (iPACKMAN)
				{
					Console.WriteLine("Not yet implemented");
					Console.ReadLine();
				}
				else
				{
					int offset = txobDataOffset;
					for(int tileY = 0; tileY < (height / 8); tileY++)
					{
						for(int tileX = 0; tileX < (width / 8); tileX++)
						{
							for(int i = 0; i < 64; i++)
							{
								int x = TileOrder[i] % 8;
								int y = (TileOrder[i] - x) / 8;
								int outOffset = ((tileX * 8) + x + (((height - 1) - ((tileY * 8) + y)) * width)) * 4;
								switch(format)
								{
									case 0: // R8G8B8A8
										Buffer.BlockCopy(cgfx, offset + 1, outByte, outOffset, 3);
										outByte[outOffset + 3] = cgfx[offset];
										offset += 4;
										break;
									case 1: // R8G8B8 (Semi-transparent)
										Buffer.BlockCopy(cgfx, offset, outByte, outOffset, 3);
										outByte[outOffset + 3] = 0xFF;
										break;
									case 2: // R5G5B5A1
									default:
										Console.WriteLine("Not yet implemented");
										break;

								}
							}
						}
					}
					//outByte;
				}

				Bitmap img = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				BitmapData imgData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
				System.Runtime.InteropServices.Marshal.Copy(outByte, 0, imgData.Scan0, outByte.Length);
				img.UnlockBits(imgData);
				img.RotateFlip(RotateFlipType.RotateNoneFlipY);
				img.Save("C:\\Users\\Mikaela\\Desktop\\image.png", ImageFormat.Png);
				Console.WriteLine("Saved!");
				Console.ReadLine();
				//Microsoft.DirectX.Direct3D.Texture tex = new Microsoft.DirectX.Direct3D.Texture(new Microsoft.DirectX.Direct3D.Device(0, Microsoft.DirectX.Direct3D.DeviceType.Hardware, Picture.Handle,)
				// Device = New Device(0, DeviceType.Hardware, Picture.Handle, CreateFlags.HardwareVertexProcessing, Present)
			}



			*/
			Console.ReadLine();
			
		}
		
	}
}
