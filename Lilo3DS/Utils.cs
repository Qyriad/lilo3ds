﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lilo3DS
{
	internal enum HexOutputType
	{
		AsHexString,
		AsASCIIString
	}

	internal enum Endianness
	{
		LittleEndian,
		BigEndian
	}

	internal static class Utils
	{
		internal static Endianness GlobalEndianness = Endianness.LittleEndian;

		internal static int[] TileOrder =
		{
			0, 1, 8, 9, 2, 3, 10, 11,
			16, 17, 24, 25, 18, 19, 26, 27,
			4, 5, 12, 13, 6, 7, 14, 15,
			20, 21, 28, 29, 22, 23, 30, 31,
			32, 33, 40, 41, 34, 35, 42, 43,
			48, 49, 56, 57, 50, 51, 58, 59,
			36, 37, 44, 45, 38, 39, 46, 47,
			52, 53, 60, 61, 54, 55, 62, 63
		};

		internal static int[][] ModulationTable =
		{
			new int[]{2, 8, -2, -8},
			new int[]{5, 17, -5, -17},
			new int[]{9, 29, -9, -29},
			new int[]{13, 42, -13, -42},
			new int[]{18, 60, -18, -60},
			new int[]{24, 80, -24, -80},
			new int[]{33, 106, -33, -106},
			new int[]{47, 183, -47, -183}
		};

		internal static T[] SubArray<T>(this T[] data, int index, int length)
		{
			T[] result = new T[length];
			Array.Copy(data, index, result, 0, length);
			return result;
		}

		internal static string GetHexValue(byte[] data, int offset, int length, HexOutputType outputType)
		{
			if(outputType == HexOutputType.AsHexString)
			{
				// Convert base 10 byte value to base 16 value as string
				string s = "";
				for(int i = offset; i < offset + length; i++)
				{
					s += data[i].ToString("X2");
				}
				return s;
			}
			else
			{
				string s = "";
				// Convert base 10 byte values to ascii string
				for(int i = offset; i < offset + length; i++)
				{
					s += (char)data[i];
				}
				return s;
			}
		}

		internal static long GetHexValue(byte[] data, int offset, int length)
		{
			string s = "";
			long value = 0;
			if(GlobalEndianness == Endianness.BigEndian)
			{
				for(int i = offset; i < offset + length; i++)
				{
					s += data[i].ToString("X2"); // Format: 2 digit hex
				}
			}
			else
			{
				for(int i = offset + length - 1; i >= offset; i--)
				{
					s += data[i].ToString("X2"); // Format: 2 digit hex
				}
			}
			value = Convert.ToInt64(s, 16);
			return value;
		}

		internal static string ReadString(byte[] data, int offset, char terminator = '\0')
		{
			string s = "";
			while(data[offset] != terminator)
			{
				s += (char)data[offset++];
			}

			return s;
		}
	}
}
