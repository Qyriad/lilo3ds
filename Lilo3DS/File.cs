﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lilo3DS
{
	abstract class File
	{
		//protected internal static Type[] CONTAINING_TYPES { get; protected set; }
		/*protected internal Type[] CONTAINING_FILE_TYPES
		{
			get { return CONTAINING_TYPES; }
		}
		*/
		//protected internal Dictionary<Type, Dictionary<Type, ...>[]>[] CONTAINING_TYPE_HIERARCHY;
		//public static Hierarchy<Type[]> CONTAINING_TYPES { get; set; }
		//protected internal Hierarchy<Type[]> CONTAINING_TYPE_HIERARCHY { get; }
		//protected internal virtual Hierarchy<Type[]> CONTAINING_TYPE_HIERARCHY { get; }

		protected internal Offset Offset { get; protected set; }
		/*
		protected internal Offset ConvertSelfRelativeOffset(Offset selfRelativeOffsetOffset)
		{
			return new Offset(this.Offset.Value + selfRelativeOffsetOffset, selfRelativeOffsetOffset.RelativeTo);
		}
		*/
		protected internal File Container { get; protected set; }

		protected internal File(Offset containerRelativeOffset)
		{
			this.Offset = containerRelativeOffset;
			this.Container = containerRelativeOffset.RelativeTo;
		}
	}
}
