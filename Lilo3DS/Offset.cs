﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Qyriad.Lilo3DS
{
	//[System.Diagnostics.DebuggerDisplay("[{\"0x\" + Value.ToString(\"X2\"),nq}, {RelativeTo}]")]
	[System.Diagnostics.DebuggerDisplay("{DebuggerDisplay,nq}")]
	internal struct Offset // Used to store the offset of its owner object relative to Offset.RelativeTo
	{
		// Readonly properties outside of 'this'
		internal int Value { get; private set; }
		internal Qyriad.Lilo3DS.File RelativeTo { get; private set; }

		//public bool AutoExpand { [CompilerGeneratedAttribute] get; [CompilerGeneratedAttribute] set;  }

		internal static Offset NULL_OFFSET { get; set; } = new Offset(0, null);
		
		internal Offset(Offset offset)// : this()
		{
			this.Value = offset.Value;
			this.RelativeTo = offset.RelativeTo;
		}

		internal Offset(int OffsetValue, Qyriad.Lilo3DS.File ObjectOffsetIsRelativeTo)// : this()
		{
			this.Value = OffsetValue;
			this.RelativeTo = ObjectOffsetIsRelativeTo;
		}
		
		private string DebuggerDisplay
		{
			get
			{
				string relString = (RelativeTo == null ? ("null") : ("{" + (RelativeTo.ToString())) + "}");
				return string.Format("{{0x{0}, {1}}}", Value.ToString("X2"), relString);
			}
		}

		/*
		internal Offset Unwind()
		{
			return new Offset(this.Value + this.RelativeTo.Offset.Value);
		}
		*/
		// Equivilent to
		internal Offset Unwind() => new Offset(this.Value + ((this.RelativeTo != null) ? (this.RelativeTo.Offset.Value) : (0)), this.RelativeTo.Container);

		public static implicit operator int(Offset o) => o.Value;
		public static implicit operator Tuple<int, File>(Offset o)
		{
			return new Tuple<int, File>(o.Value, o.RelativeTo);
		}

		public void Deconstruct(out int value, out File RelativeTo)
		{
			value = this.Value;
			RelativeTo = this.RelativeTo;
		}
	}
}
